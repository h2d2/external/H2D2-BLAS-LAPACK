@echo off
setlocal

:: ---  Check environment
if [%TMP%] == []         echo TMP must be defined      && goto exit
if [%INRS_BLD%] == []    echo INRS_BLD must be defined && goto exit
if [%INRS_DEV%] == []    echo INRS_DEV must be defined && goto exit
if [%INRS_LXT%] == []    echo INRS_LXT must be defined && goto exit
if not exist %TMP%       echo %TMP% must exist      && goto exit
if not exist %INRS_BLD%  echo %INRS_BLD% must exist && goto exit
if not exist %INRS_DEV%  echo %INRS_DEV% must exist && goto exit
if not exist %INRS_LXT%  echo %INRS_LXT% must exist && goto exit

:: ---  Check arguments count
if [%2] == [] echo Usage: cpl_on.bat Compiler MPIlib [i4-i8] [supplementary-args] && goto exit

:: ---  Get arguments
set LCL_CPL=%1
set LCL_MPI=%2
set LCL_ISZ=%3
if [%LCL_ISZ%] == [i4] (
   set LCL_ISZ=fintegersize=4
   for /f "tokens=1-3*" %%a in ("%*") do ( set LCL_SUP=%%d )
) else if [%LCL_ISZ%] == [i8] (
   set LCL_ISZ=fintegersize=8
   for /f "tokens=1-3*" %%a in ("%*") do ( set LCL_SUP=%%d )
) else (
   set LCL_ISZ=fintegersize=4
   for /f "tokens=1-2*" %%a in ("%*") do ( set LCL_SUP=%%c )
)

:: ---  Calls
set SCONSTRUCT=%INRS_BLD%/MUC_Scons/SConstruct
call scons -Q --file=%SCONSTRUCT% compilers=%LCL_CPL% mpilibs=%LCL_MPI%  %LCL_ISZ%  builds=debug %argsSup%
call scons -Q --file=%SCONSTRUCT% compilers=%LCL_CPL% mpilibs=%LCL_MPI%  %LCL_ISZ%  builds=release %argsSup%
call scons -Q --file=%SCONSTRUCT% compilers=%LCL_CPL% mpilibs=%LCL_MPI%  %LCL_ISZ%  builds=debug   forcestatic=on %argsSup%
call scons -Q --file=%SCONSTRUCT% compilers=%LCL_CPL% mpilibs=%LCL_MPI%  %LCL_ISZ%  builds=release forcestatic=on %argsSup%

:exit
endlocal
